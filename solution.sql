JOSHUA G. ORBISO BSIT-3 | G1

1.
SELECT * FROM artists WHERE name LIKE "%D%";

2.
SELECT * FROM songs WHERE length < 230;

3.
SELECT album_title, song_name, length FROM albums
    JOIN songs ON albums.id = songs.album_id;

4.
SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id
    WHERE album_title LIKE "%A%";

5.
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

6.
SELECT * FROM albums
    JOIN songs ON albums.id = songs.album_id
    ORDER BY album_title DESC, song_name;